<?php

    $moviesdata=file_get_contents("movies.json");
    // file_get_contents function reads entire file source or part of it and returns as a PHP string data.
    // echo $moviesdata;
    $moviejson=json_decode($moviesdata,true);
    // echo $moviejson;
    // json_decode function takes a JSON string and converts it into a PHP variable. 
    
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>My movies</title>
</head>
<body>
    <div id="container">
        <h1>My favorite movies</h1>
            <tr>
                <?php
                    foreach($moviejson["movies"] as $key=>$value){
                        echo '<h4>'.($key+1)." : ".$value['title'].'</h4>';
                        echo '<li>'.$value['actor'].'</li>';
                        echo '<li>'.$value['director'].'</li>';
                        echo '<li>'.$value['genre'].'</li>';
                        echo '<li>'.$value['year'].'</li>';
                    }
                ?>
            </tr>
    </div>
</body>
</html>