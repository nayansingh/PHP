<?php

    class Car{
        public $make,$color;
        private $model;

        // we cannot create more than one contructor in php
        
        public function __construct($make,$model,$color){       
            $this->make=$make;
            $this->model=$model;
            $this->color=$color;
        }

        public function start(){
            return 'car starting...';
        }

        public function setModel($model){
            $this->model=$model;
        }

        public function getModel(){
            return $this->model;
        }

        public function getDetails(){
            echo $this->make;
            echo $this->model;
            echo $this->color;
        }
    }

?>