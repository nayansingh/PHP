<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Basics</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    
<?php
    $text="Hello World "; 
    $num=456;
    echo $text.($num+$num);

?>

<form action="" method="get"> 
    <fieldset>
        <legend>Enter info..</legend>
        <label>Name: <input type="text" name="text" size="30" maxlength="8"/></label>         
            <input type="submit" value="get"/>
    </fieldset>
</form>

<form action="" method="post"> 
    <fieldset>
        <legend>Enter info..</legend>
        <label>Age: <input type="text" name="txt" size="30" maxlength="8"/></label>         
            <input type="submit" value=" post "/>
    </fieldset>
</form>
</body>

<?php           // get and post are global variables as they get data from browser

echo $_GET['text'].'</br>';           // text is written because it fetches data from second input 
// whose name is text, as name is provided here

echo $_POST['txt']; 


?>

</html>