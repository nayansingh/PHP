<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Arrays</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<?php
    $countries=array(
        ["Delhi ","Mumbai ","Kolkata "],
        ["Chicago ","San Fransisco "],
        ["London ","Frankfurt ","Manchester ","Big Ben "]);
    
    foreach($countries as $cities){
        echo '<br/><tr>';
        foreach($cities as $city){
            echo "<td>{$city}</td>";
        }
        echo '</tr>';
    }
    
    echo '<br/> <br/> <br/>';

    $car=array('Make'=>'toyota','Model'=>'camry','Color'=>'black');
    
    foreach($car as $key=>$val){
        echo '<br/>'.ucwords($key).' : '.ucwords($val);
    }
    echo '<br/>';
    echo count($car);
    echo '<br/>';
    sort($cities);
    print_r($cities);

    // var_dump($cities);        // used to tell type of data
?>

</body>
</html>